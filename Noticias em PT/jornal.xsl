<?xml version="1.0" encoding="UTF-8"?>
<!-- DWXMLSource="relatorio_dados.xml" -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:ns="http://xml.dei.isep.ipp.pt/schema/LPROG_NEWS">
    <xsl:output method="html"/>

    <xsl:template match="/ns:jornal">
        <html>
            <head>
                <title>LPROG NEWS</title>
            </head>
            <body>
                <xsl:apply-templates select="ns:Noticias"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="ns:Noticias">
        <table>
            <xsl:apply-templates select="ns:Noticia"/>
        </table>
    </xsl:template>
    
    <xsl:template match="ns:Noticia">
        <tr>
            <td>
                <xsl:apply-templates select="ns:Texto"/>
            </td>
            <td>
                <table width="500" border="1" align="center" bordercolor="#0066FF" cellspacing="10">
                    <tr>
                        <p align="center">
                            <xsl:element name="img">
                                <xsl:attribute name="src">
                                    <xsl:value-of select="ns:Imagem"/>
                                </xsl:attribute>
                            </xsl:element>
                        </p>
                    </tr>
                </table>
                <br/>
                <br/>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" align="right">
                    <tr>
                        <tr>
                            <td align="right" colspan="3">
                                <font face="technic" size="4" color="#236B8E" >
                                    <b>Noticia redigida por:</b>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <font face="technic" size="3" >
                                    <b>Nome: </b>
                                </font>
                            </td>
                            <td>
                                <font face="technic" size="3" >
                                    <xsl:value-of select="ns:Autor"/>
                                </font>
                            </td>
                        </tr>		
                    </tr>		
                </table>
            </td>
            <td>
                <font face="technic" size="2">
                    <div align="left">
                        <blockquote>
                            Data:
                            <xsl:value-of select="ns:DataHora"/>
                        </blockquote>
                    </div>
                </font>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="ns:Texto">
        <xsl:apply-templates select="ns:Lingua"/>
    </xsl:template>
    
    
    <xsl:template match="ns:Lingua">
	<xsl:if test="@idLingua = 'PT'">
        <div align="center">
            <font face="technic" size="5" color="#bfb8b8">
                <b>
                    <xsl:value-of select="ns:Titulo"/>
                </b>
            </font>
        </div>
        <div align="left">
            <blockquote>
                <font face="technic" size="4" color="#236B8E">
                    <i>
                        <b>
                            <xsl:value-of select="ns:Parcial"/>
                        </b>
                    </i>
                </font>
            </blockquote>
        </div>
        <div align="left">
            <blockquote>
                <font face="technic" size="3" color="#236B8E">
                    <i>
                        <xsl:value-of select="ns:Total"/>
                    </i>
                </font>
            </blockquote>
        </div>
		</xsl:if>
    </xsl:template>
    
</xsl:stylesheet>