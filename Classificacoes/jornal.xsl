<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:ns="http://xml.dei.isep.ipp.pt/schema/LPROG_NEWS">
    <xsl:output method="html"/>

    <xsl:template match="/ns:jornal">
        <html>
            <head>
                <title>LPROG NEWS</title>
            </head>
            <body>
                <div align="center">
				<font face="technic" size="7" color="#bfb8b8">
				<p align="center">Lista de Campeonatos</p>
				</font>
				</div>
				
                <xsl:apply-templates select="ns:Campeonatos"/>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="ns:Campeonatos">
        	<div align="center">
			<font face="technic" size="5" color="orange">
			<p align="center">Campeonato <xsl:value-of select="ns:Tipo"/></p>
			</font>
			</div>
            <br/>
            <xsl:apply-templates select="ns:Campeonato"/>
    </xsl:template>


    <xsl:template match="ns:Campeonato">
        <xsl:apply-templates select="ns:TabelaClassificacao"/>
    </xsl:template>

    <xsl:template match="ns:TabelaClassificacao">

<div align="center">
			<font face="technic" size="4" color="blue">
			<p align="center">Modalidade <xsl:value-of select="ns:Modalidade"/></p>
			</font>
			</div>
            
        <xsl:apply-templates select="ns:Linhas"/>  
	
    </xsl:template>

    <xsl:template match="ns:Linhas">
	<table align="center" border="1">	
        <tr bgcolor="#9acd32">
            <th>Posição</th>
            <th>Clube</th>
            <th>Pontos</th>
        </tr>
        <xsl:apply-templates select="ns:Linha"/>
		</table>
    </xsl:template>

    <xsl:template match="ns:Linha">
        <tr>
            <td align="center"><xsl:value-of select="ns:Posicao"/></td>
            <td align="center"><xsl:value-of select="ns:Clube"/></td>
            <td align="right"><xsl:value-of select="ns:Pontos"/></td>
			
        </tr>
    </xsl:template>

</xsl:stylesheet> 