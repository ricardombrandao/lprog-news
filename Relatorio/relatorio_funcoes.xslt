<!-- DWXMLSource="relatorio_dados.xml" -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:output method="html"/>

<xsl:template name="RELATORIOTEMPLATES">
	<html>
		<body >
      
        <xsl:apply-templates select="relatorio/paginarosto"/>
        <xsl:apply-templates select="relatorio/corpo/introducao"/>
        <xsl:apply-templates select="relatorio/corpo/seccoes/analise"/>
        <xsl:apply-templates select="relatorio/corpo/seccoes/linguagem"/>
        <xsl:apply-templates select="relatorio/corpo/seccoes/transformacoes"/>
        <xsl:apply-templates select="relatorio/corpo/conclusao"/>
        <xsl:apply-templates select="relatorio/corpo/referencias"/>
		</body>
	</html>
</xsl:template>

<xsl:template match="relatorio/paginarosto">
	<xsl:apply-templates select="tema"/>
    <xsl:apply-templates select="autor"/>
	<xsl:apply-templates select="turmaPL"/>
	<xsl:apply-templates select="profPL" />
    <xsl:apply-templates select="data"/>
</xsl:template>

<xsl:template match="relatorio/paginarosto/turmaPL">
	<table border="0" align="Center">
	<tr>
			<td align="left">
				<font face="technic" size="3" >
					<b>Turma PL:</b>
				</font>
            </td>
			<td>
				<font face="technic" size="3">
					<xsl:value-of select="."/>
				</font>
			</td>
	</tr>
	</table>
</xsl:template>

<xsl:template match="relatorio/paginarosto/profPL">
	<table border="0" align="Center">
	<tr>
		<td align="left">
			<font face="technic" size="3" >
				<b>Prof PL:</b>
			</font>
         </td>
		<td>
			<font face="technic" size="3">
				<xsl:value-of select="."/>
			</font>
		</td>
	</tr>
	</table>
<br/><br/><br/><br/>
</xsl:template>
  
<xsl:template match="relatorio/paginarosto/tema">
  <table width="900" border="0" align="center" bordercolor="#bfb8b8" cellspacing="10">
    <tr>
      <br/>
      <br/>
      <div align="center">
        <font face="technic" size="7" color="#bfb8b8">
          <xsl:value-of select="."/>
        </font>
      </div>
    </tr>
    </table>
      <p align="center">
        <img src="dei_logo.png" width="830" height="334"/>
      </p>

</xsl:template>

<xsl:template match="relatorio/corpo/seccoes/analise/bloco/figura">
  <table width="940" border="1" align="center" bordercolor="#0066FF" cellspacing="10">
    <tr>
      <p align="center">
        <img src="diagrama.png"/>
      </p>
    </tr>
    <tr bgcolor="#c8dae2">
      <b>
        
        <p align="center">
          <font face="technic" size="3">
            <xsl:value-of select="."/>
          </font>
        </p>
      </b>
    </tr>
  </table>
</xsl:template>

<xsl:template match="relatorio/paginarosto/autor">
    
		<table border="0" align="Center">
      <tr>
        
        <tr>
          <td align="left" colspan="3">
            <font face="technic" size="4" color="#236B8E" >
              <b>Trabalho elaborado por:</b>
            </font>
          </td>
        </tr>

        <xsl:for-each select="aluno">
          <tr>
        <td align="left">
          <font face="technic" size="3" >
            <b>Nome: </b>
          </font>
        </td>
       
            <td>
              <font face="technic" size="3" >
                <xsl:value-of select="nome"/>
              </font>
            </td>




            <td align="left">
              <font face="technic" size="3" >
                <b>Número:</b>
              </font>
            </td>

            <td align="left">
              <font face="technic" size="3" >
                <xsl:value-of select="numero"/>
              </font>
            </td>


            <td align="left">
              <font face="technic" size="3" >
                <b>Mail:</b>
              </font>
            </td>

            <td>
              <font face="technic" size="3">
                <xsl:value-of select="mail"/>
              </font>
            </td>
          </tr>
        </xsl:for-each>
		
      </tr>
			
	</table>
</xsl:template>

<xsl:template match="relatorio/paginarosto/data">
	<font face="technic" size="3"><div align="right">
    <blockquote>
      <b>
        Data de Entrega do Trabalho:
      </b>
      <xsl:value-of select="."/>
    </blockquote>
  </div>
  </font>
</xsl:template>

<xsl:template match="introducao">
	<blockquote>
		 <font align="left" face="technic" size="5" color="#236B8E"> <b>	<xsl:value-of select="@tituloseccao"/> </b></font>
	</blockquote>
	<xsl:apply-templates select="bloco"/>
</xsl:template>

<xsl:template match="bloco">
<xsl:apply-templates select="listaitens|paragrafo|figura|codigo"/>
</xsl:template>

<xsl:template match="analise">
	<blockquote>
		  <font face="technic" size="5" color="#236B8E"> <b>	<xsl:value-of select="@tituloseccao"/> </b></font>
	</blockquote>
	<xsl:apply-templates select="bloco"/>
	<br/>
</xsl:template>

<xsl:template match="linguagem">
	<blockquote>
		 <font face="technic" size="5" color="#236B8E"> <b>	<xsl:value-of select="@tituloseccao"/> </b></font>
	</blockquote>
	<xsl:apply-templates select="bloco"/>
	<br/>
</xsl:template>

<xsl:template match="transformacoes">
	<blockquote>
		 <font face="technic" size="5" color="#236B8E"> <b>	<xsl:value-of select="@tituloseccao"/> </b></font>
	</blockquote>
	<xsl:apply-templates select="bloco"/>
	<br/>
</xsl:template>

<xsl:template match="conclusao">
	<blockquote>
		 <font face="technic" size="5" color="#236B8E"> <b>	<xsl:value-of select="@tituloseccao"/> </b></font>
	</blockquote>
	<xsl:apply-templates select="bloco"/>
	<br/>
</xsl:template>

<xsl:template match="paragrafo">
	<blockquote>
		<blockquote>
			<p>
				<font face="technic" size="3"><div align="justify"><xsl:value-of select="."/></div></font>				
			</p>
		</blockquote>
	</blockquote>
</xsl:template>

<xsl:template match="codigo">
<ul>
	<ul>
		<ul>
		  <font face="technic" size="3" ><b>Excerto de código número:</b></font>
		  <font face="technic" size="3" color="#FF0000"> <i><xsl:value-of select="@id"/> </i></font>
	    <p>
		    <font face="technic" size="3" color="#3333FF"> <i><xsl:value-of select="."/> </i></font> 
	    </p>
		</ul>
	</ul>
</ul>

</xsl:template>

<xsl:template match="referencias">
	<blockquote>
		 <font face="technic" size="5" color="#236B8E"> <b>	Referências </b></font>
	</blockquote>
	<xsl:apply-templates select="refWeb"/>
</xsl:template>

<xsl:template match="refWeb">
  <blockquote>
	  <blockquote>
      <font face="technic" size="3" ><u>Referências web:</u></font>
		    <ul>
			   <ul>
			    	<li>
				    	<p>
				  	  	<font face="technic" size="3" ><b>URL:</b></font>
					  	  <font face="technic" size="3" ><xsl:value-of select="URL"/></font>
					   </p>
					    <p>
					    	<font face="technic" size="3" ><b>Descrição:</b></font>
					    	<font face="technic" size="3" ><xsl:value-of select="descricao"/></font>
					    </p>
						<p>
					    	<font face="technic" size="3" ><b>Consultado Em:</b></font>
					    	<font face="technic" size="3" ><xsl:value-of select="consultadoEm"/></font>
					    </p>
			    	</li>
		      </ul>
	      </ul>
    </blockquote>
  </blockquote>
</xsl:template>

<xsl:template match="listaitens">
	<xsl:for-each select="item">
		<ul>
			<ul>
				<ul>
				<li>
					<p>
						<font face="technic" size="3"><div align="justify"><xsl:value-of select="."/></div></font>
					</p>
				</li>
				</ul>
			</ul>
		</ul>
	</xsl:for-each>
</xsl:template>
  
</xsl:stylesheet>
